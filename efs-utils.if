
## <summary>policy for efs-utils</summary>

########################################
## <summary>
##	Execute efs-utils_exec_t in the efs-utils domain.
## </summary>
## <param name="domain">
## <summary>
##	Domain allowed to transition.
## </summary>
## </param>
#
interface(`efs-utils_domtrans',`
	gen_require(`
		type efs-utils_t, efs-utils_exec_t;
	')

	corecmd_search_bin($1)
	domtrans_pattern($1, efs-utils_exec_t, efs-utils_t)
')

######################################
## <summary>
##	Execute efs-utils in the caller domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`efs-utils_exec',`
	gen_require(`
		type efs-utils_exec_t;
	')

	corecmd_search_bin($1)
	can_exec($1, efs-utils_exec_t)
')
########################################
## <summary>
##	Read efs-utils's log files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
## <rolecap/>
#
interface(`efs-utils_read_log',`
	gen_require(`
		type efs-utils_log_t;
	')

	logging_search_logs($1)
	read_files_pattern($1, efs-utils_log_t, efs-utils_log_t)
')

########################################
## <summary>
##	Append to efs-utils log files.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`efs-utils_append_log',`
	gen_require(`
		type efs-utils_log_t;
	')

	logging_search_logs($1)
	append_files_pattern($1, efs-utils_log_t, efs-utils_log_t)
')

########################################
## <summary>
##	Manage efs-utils log files
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
#
interface(`efs-utils_manage_log',`
	gen_require(`
		type efs-utils_log_t;
	')

	logging_search_logs($1)
	manage_dirs_pattern($1, efs-utils_log_t, efs-utils_log_t)
	manage_files_pattern($1, efs-utils_log_t, efs-utils_log_t)
	manage_lnk_files_pattern($1, efs-utils_log_t, efs-utils_log_t)
')
########################################
## <summary>
##	Execute efs-utils server in the efs-utils domain.
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed to transition.
##	</summary>
## </param>
#
interface(`efs-utils_systemctl',`
	gen_require(`
		type efs-utils_t;
		type efs-utils_unit_file_t;
	')

	systemd_exec_systemctl($1)
        systemd_read_fifo_file_passwd_run($1)
	allow $1 efs-utils_unit_file_t:file read_file_perms;
	allow $1 efs-utils_unit_file_t:service manage_service_perms;

	ps_process_pattern($1, efs-utils_t)
')


########################################
## <summary>
##	All of the rules required to administrate
##	an efs-utils environment
## </summary>
## <param name="domain">
##	<summary>
##	Domain allowed access.
##	</summary>
## </param>
## <param name="role">
##	<summary>
##	Role allowed access.
##	</summary>
## </param>
## <rolecap/>
#
interface(`efs-utils_admin',`
	gen_require(`
		type efs-utils_t;
		type efs-utils_log_t;
	type efs-utils_unit_file_t;
	')

	allow $1 efs-utils_t:process { signal_perms };
	ps_process_pattern($1, efs-utils_t)

    tunable_policy(`deny_ptrace',`',`
        allow $1 efs-utils_t:process ptrace;
    ')

	logging_search_logs($1)
	admin_pattern($1, efs-utils_log_t)

	efs-utils_systemctl($1)
	admin_pattern($1, efs-utils_unit_file_t)
	allow $1 efs-utils_unit_file_t:service all_service_perms;
	optional_policy(`
		systemd_passwd_agent_exec($1)
		systemd_read_fifo_file_passwd_run($1)
	')
')
